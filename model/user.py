from enum import Enum

from marshmallow.base import FieldABC


class Gender(Enum):
    male = 1,
    female = 2


class Status(Enum):
    active = 1,
    inactive = 2


class User():
    def __init__(self, id, name, email, gender, status) -> None:
        super().__init__()
        self.__id = id
        self.name = name
        self.email = email
        self.gender = gender
        self.status = status

    @property
    def id(self):
        return self.__id

    def __repr__(self) -> str:
        return "model.User({}, {}, {}, {}, {})".format(
            self.__id, self.name, self.email, self.gender, self.status
        )


