from marshmallow import Schema, fields, post_load

from model.user import Gender, Status, User


class UserSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    email = fields.Email()
    gender = fields.Enum(Gender)
    status = fields.Enum(Status)

    @post_load
    def make_user(self, data, **kwargs):
        return User(**data)


