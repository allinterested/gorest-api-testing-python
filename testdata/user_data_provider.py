import random


class UserDataProvider:

    @staticmethod
    def create_user_payload():
        email = 'ivansemail' + str(random.randrange(20089)) + '@gmail.com'
        user_payload = {
            'name': 'Ivan Paulouski',
            'email': email,
            'gender': 'male',
            'status': random.choice(['active', 'inactive'])
        }
        return user_payload

    @staticmethod
    def create_user_payload_for_update():
        original_user = UserDataProvider.create_user_payload()
        original_user['name'] = 'John Doe'
        return original_user
