import logging
import pytest
from assertpy import assert_that
from api.user_api import UserApi
from testdata.user_data_provider import UserDataProvider

logging.basicConfig(level=logging.INFO, format='%(levelname)s:%(module)s:%(message)s')


@pytest.mark.usefixtures("gorest_token")
class TestUserApi:
    host = 'https://gorest.co.in/public/v2'
    user_api = None
    user_payload = None

    @pytest.fixture(autouse=True)
    def setup(self, gorest_token):
        self.user_api = UserApi(self.host, gorest_token)
        self.user_payload = UserDataProvider.create_user_payload()

    def test_should_not_create_user_without_auth_token(self):
        user_api = UserApi(self.host, '')
        status_code, body = user_api.create_user(self.user_payload)
        assert_that(status_code).is_equal_to(401)

    def test_should_fetch_users_information_without_auth_token(self):
        users = self.user_api.get_all_users()
        assert_that(users[0].id).is_not_none()

    def test_should_get_all_users_information(self):
        users = self.user_api.get_all_users()
        assert_that(users[0].id).is_not_none()

    def test_should_get_user_information(self):
        user_id = self.user_api.create_user(self.user_payload).id
        resp = self.user_api.get_user(user_id)
        assert_that(resp.name).is_not_none()

    def test_should_create_user(self):
        resp = self.user_api.create_user(self.user_payload)
        user_id = resp.id
        assert_that(user_id).is_not_none()

    def test_should_delete_user(self):
        user_id = self.user_api.create_user(self.user_payload).id
        resp = self.user_api.delete_user(user_id)
        assert_that(resp.status_code).is_equal_to(204)

    def test_should_update_existing_user(self):
        user_id = self.user_api.create_user(self.user_payload).id
        new_user = UserDataProvider.create_user_payload_for_update()
        updated_user = self.user_api.update_user(user_id, new_user)
        assert_that(updated_user.name).is_equal_to(new_user['name'])

    def test_should_return_not_found_for_non_existing_user(self):
        user_id = 000000
        status_code, resp = self.user_api.get_user(user_id)
        msg = resp['message']
        assert_that(msg).is_equal_to('Resource not found')
