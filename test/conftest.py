import pytest


def pytest_addoption(parser):
    parser.addoption("--gorest_token", action="store", default="", help="GOREST service authorization token")

@pytest.fixture
def gorest_token(request):
    return request.config.getoption("--gorest_token")

#