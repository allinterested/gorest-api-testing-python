import json
import logging

import requests

from schema.user_schema import UserSchema

logging.basicConfig(level=logging.INFO, format='%(levelname)s:%(module)s:%(message)s')
class UserApi:
    def __init__(self, host, auth_token) -> None:
        super().__init__()
        self.headers = {'Authorization': 'Bearer ' + auth_token}
        self.endpoint = '/users'
        self.url = host + self.endpoint

    def get_all_users(self):
        resp = requests.get(self.url, headers=self.headers)
        users_json = resp.json()
        logging.info("Users: {}".format(json.dumps(users_json, indent=4)))
        users = UserSchema(many=True).load(users_json)
        return users

    def get_user(self, user_id):
        resp = requests.get(self.url + '/' + str(user_id), headers=self.headers)
        if resp.status_code == 200:
            user = UserSchema().load(resp.json())
            logging.info("User object: {}".format(user.__repr__()))
            return user
        else:
            return resp.status_code, resp.json()

    def create_user(self, user_data):
        resp = requests.post(self.url, headers=self.headers, data=user_data)
        if resp.status_code == 201:
            user = UserSchema().load(resp.json())
            logging.info("User object: {}".format(user.__repr__()))
            return user
        else:
            return resp.status_code, json.dumps(resp.json())

    def delete_user(self, user_id):
        logging.info("Deleting user with id '{}'".format(user_id))
        return requests.delete(self.url + '/' + str(user_id), headers=self.headers)

    def update_user(self, user_id, user_payload):
        logging.info("Updating user with id '{}' with new data: {}".format(user_id, user_payload))
        resp = requests.put(self.url + '/' + str(user_id), headers=self.headers, data=user_payload)
        user = UserSchema().load(resp.json())
        logging.info("Updated user: {}".format(user.__repr__()))
        return user
