<h2>Lightweight and robust framework for API automation on Python</h2>
</br>
<h3>Project overview</h3>
This GitLab repository contains a comprehensive test automation project showcasing proficiency in REST API test automation using Python, primarily leveraging the Requests and Assertpy libraries. The project demonstrate the ability to create robust, automated tests for REST API, verifying different response codes and data. It uses Python's Requests library to send HTTP requests and Assertpy for advnaced assertion capabilities. It is also use Marshmallow library for more convenient serialization/deserialization process. The neatly organized codebase and well-documented steps provide a clear understanding of the test case creation and execution process. This repository serves as an example of solution for maintainable and scalable API test automation.
<p>
<i>Sample test report can be found in CI/CD artifacts section</i>
<p>
<h3>Tools and libs</h3>
<ul>
<li>pytest - test runner</li>
<li>requests - http client</li>
<li>assertpy - test assertions</li>
<li>pytest-html - test report generation</li>
<li>marshmallow - JSON serialization/deserialization</li>
</ul>

<h3>How to run</h3>
<ol>
<li>Download the project</li>
<li>Run <b>pip install -r requirements.txt</b></li>
<li>Run <b>pytest --html=report.html --gorest_token=</b><i>your_gorest_api_token</i></li>
<li>Check <b>report.html</b> file for test execution results</li>
</ol>
